import assert from 'assert';
import CircularBuffer from '../lib';

describe('m-circular-buffer', function () {
  it('should set length', function () {
    let buffer = new CircularBuffer(8);
    assert.equal(0, buffer.length);
  });

  it('should push/pull data to/from buffer', function () {
    let buffer = new CircularBuffer(4);

    assert.equal(0, buffer._start); //0000
    assert.equal(0, buffer._end);
    assert.equal(0, buffer.length);

    assert(buffer.push(new Buffer('abc'))); //abc0
    assert.equal(0, buffer._start);
    assert.equal(3, buffer._end);
    assert.equal(3, buffer.length);

    assert(!buffer.push(new Buffer('abc'))); //abc0
    assert.equal(0, buffer._start);
    assert.equal(3, buffer._end);
    assert.equal(3, buffer.length);

    assert(!buffer.unshift(4, null)); //abc0
    assert.equal(0, buffer._start);
    assert.equal(3, buffer._end);
    assert.equal(3, buffer.length);

    assert(buffer.unshift(2, function (chunk) { //00c0
      assert.equal(0, chunk.compare(new Buffer('ab')));
    }));
    assert.equal(2, buffer._start);
    assert.equal(3, buffer._end);
    assert.equal(1, buffer.length);

    assert(!buffer.unshift(2, null)); //00c0
    assert.equal(2, buffer._start);
    assert.equal(3, buffer._end);
    assert.equal(1, buffer.length);

    assert(buffer.push(new Buffer('abc'))); //bcca
    assert.equal(2, buffer._start);
    assert.equal(2, buffer._end);
    assert.equal(4, buffer.length);

    assert(buffer.unshift(2, function (chunk) { //bc00
      assert.equal(0, chunk.compare(new Buffer('ca')));
    }));
    assert.equal(0, buffer._start);
    assert.equal(2, buffer._end);
    assert.equal(2, buffer.length);

    assert(buffer.unshift(1, function (chunk) { //0c00
      assert.equal(0, chunk.compare(new Buffer('b')));
    }));
    assert.equal(1, buffer._start);
    assert.equal(2, buffer._end);
    assert.equal(1, buffer.length);

    assert(buffer.push(new Buffer('xyz'))); //zcxy
    assert.equal(1, buffer._start);
    assert.equal(1, buffer._end);
    assert.equal(4, buffer.length);

    let firstPass = true;
    assert(buffer.unshift(4, function (chunk) { //0000
      if (firstPass) {
        assert.equal(0, chunk.compare(new Buffer('cxy')));
        firstPass = false;
      } else {
        assert.equal(0, chunk.compare(new Buffer('z')));
      }
    }));
    assert.equal(1, buffer._start);
    assert.equal(1, buffer._end);
    assert.equal(0, buffer.length);
  });
});
