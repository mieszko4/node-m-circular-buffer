function CircularBuffer(
  size = 64 //bytes
) {
  if (!(this instanceof CircularBuffer)) {
    return new CircularBuffer();
  }

  this._buffer = new Buffer(size);
  this._start = 0;
  this._end = 0;
  this._length = 0;
}

CircularBuffer.prototype.push = function (chunk) {
  if (chunk.length > this._buffer.length - this._length) {
    return false;
  }

  if (this._end < this._start || this._end + chunk.length <= this._buffer.length) {
    chunk.copy(this._buffer, this._end, 0, chunk.length);
  } else {
    chunk.copy(this._buffer, this._end, 0, this._buffer.length - this._end);
    chunk.copy(this._buffer, 0, this._buffer.length - this._end, chunk.length);
  }

  this._length += chunk.length;
  this._end = (this._end + chunk.length) % this._buffer.length;

  return true;
};

CircularBuffer.prototype.unshift = function (n, consume) {
  if (n > this._length) {
    return false;
  }

  if (this._start < this._end || this._start + n <= this._buffer.length) {
    consume.call(null, this._buffer.slice(this._start, this._start + n));
  } else {
    consume.call(null, this._buffer.slice(this._start, this._buffer.length));
    consume.call(null, this._buffer.slice(0, n - (this._buffer.length - this._start)));
  }

  this._length -= n;
  this._start = (this._start + n) % this._buffer.length;

  return true;
};

Object.defineProperty(CircularBuffer.prototype, 'length', {
  get: function () {
    return this._length;
  }
});


export default CircularBuffer;
