# m-circular-buffer
> Class that extends Buffer with reading/writing data to buffer in circular manner.

## Requirements

* ```git```
* ```npm``` and ```node.js```

## Install

```sh
$ npm install bitbucket:mieszko4/node-m-circular-buffer
```


## Usage

```js
var CircularBuffer = require('m-circular-buffer');
var buffer = new CircularBuffer(4);

buffer.push(new Buffer([1,2,3])); //true
buffer.push(new Buffer([1,2,3])); //false
buffer.push(new Buffer([3])); //true
buffer.push(new Buffer([3])); //false

buffer.unshift(9, console.log); //false
buffer.unshift(3, console.log); //true <Buffer 01 02 03>
buffer.unshift(1, console.log); //true <Buffer 03>
buffer.unshift(1, console.log); //false
```

## Development


```sh
$ git clone https://mieszko4@bitbucket.org/mieszko4/node-m-circular-buffer.git
$ cd node-m-circular-buffer
$ npm install
$ npm test
```


## License

Apache-2.0 © [Mieszko4]()
